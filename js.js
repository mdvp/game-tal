//todo
// declarar variaveis nas funcoes para ajuda no debug
// inimigos teem que disparar
// a nave ao ser atingida perde vida e volta a posicao inicial
// somar pontos por cada inimigo destruido

var canvas = document.querySelector("#canvas");
var context = canvas.getContext("2d");

var header = document.querySelector("#menu");

var cannon = {
    width: 50,
    height: 35,
    x: 0,
    y: 0,
    img: "images/cannon.jpg",
    speed: 15,
    lives: 3,
    score: 0,
    axes: {
        allowX: true,
        allowY: false
    },
    shot: {
        width: 2,
        height: 20,
        color: "green",
        speed: 20,
        x: 0,
        y: 0,
        onTheWay: false
    }
};

var keypad = {
    left: 37,
    top: 38,
    right: 39,
    bottom: 40,
    fire: 32
};

var enemyCannon = {
    width: 50,
    height: 35,
    speed: 15,
    img: "images/spaceinvader.jpg",
    x: 0,
    y: 0,
    killedPrice: 10
}; //killedPrice nao implementado

//array de enemyCannon
var invaders = new Array();

//Inicialize Components
window.onload = function () {
    header.innerHTML = cannon.score;

    document.body.style.cursor = "none";

    setCanvasToFullScreen();
    fillEnemyField(true);
    drawNewCannon();
    setIntervalMovementsToInvaders();
}

function setCanvasToFullScreen() {
    canvas.width = window.innerWidth - 10;
    canvas.height = window.innerHeight - header.clientHeight - document.querySelector("footer").clientHeight - 10;
}

function setCannonInicialPosition() {
    cannon.x = (canvas.width / 2) - cannon.width;
    cannon.y = canvas.height - cannon.height - 10;
}

function drawNewCannon() {
    setCannonInicialPosition();
    drawCannon();
}

function fillEnemyField(fromConstrutor) {
    if (fromConstrutor) {
        invaders = new Array();
        //todo eliminar elemento img dos invaders
    }

    var fieldHeight = canvas.height / 2;
    var fieldWidth = canvas.width / 2;

    //o primeiro invader comeca a 20% da medida disponivel tanto em altura como largura
    inicializeRowOfInvaders(fieldWidth * 0.2, fieldHeight * 0.2);
    inicializeRowOfInvaders(fieldWidth * 0.4, fieldHeight * 0.4);
    inicializeRowOfInvaders(fieldWidth * 0.6, fieldHeight * 0.6);
    inicializeRowOfInvaders(fieldWidth * 0.8, fieldHeight * 0.8);
}

function inicializeRowOfInvaders(startX, startY) {
    var xToFinish = canvas.width - startX; //fazer com que o final termine a mesma distancia que o inicio
    var xToFill = startX;
    var marginBetweenThem = 30;

    for (var i = 0; i <= (xToFinish / enemyCannon.width) + marginBetweenThem; i++) {
        var invader = clone(enemyCannon);

        invader.x = xToFill;
        invader.y = startY;

        invaders[invaders.length] = invader;

        xToFill += invader.width + marginBetweenThem;

        drawEnemy(invader);

        if (xToFill >= xToFinish) return;
    }
}

function drawEnemy(invader) {
    var id = invaders.indexOf(invader);
    var img = document.querySelector("#invader" + id);

    if (img == null) {
        img = document.createElement("img");
        img.id = "invader" + id;
        img.src = invader.img;
    }

    context.drawImage(img, invader.x, invader.y, invader.width, invader.height);
}

function drawCannon() {
    var img = document.querySelector("#cannon");

    //Faco a verificacao aqui por seguranca
    if (img == null) {
        img = document.createElement("img");
        img.id = "cannon";
        img.src = cannon.img;
    }

    //console.log("######### draw cannon ######### x:" + cannon.x + "; y:" + cannon.y);

    context.drawImage(img, cannon.x, cannon.y, cannon.width, cannon.height);
}

function setIntervalMovementsToInvaders() {
    setInterval(setMovementsToInvaders, 1000, false);
}

var intervalOfFireCannon = new Array();

document.onkeydown = function (e) {
    if (e.keyCode == keypad.fire && !cannon.shot.onTheWay) {
        fireCannon();
        intervalOfFireCannon[intervalOfFireCannon.length] = setInterval(fireCannon, 20);
    }

    moveCannon(e.keyCode);
}

function cannonStopShooting() {
    intervalOfFireCannon.forEach(function (item) {
        clearInterval(item);
    });

    intervalOfFireCannon = new Array();

    cannon.shot.x = 0;
    cannon.shot.y = 0;
    cannon.shot.onTheWay = false;
}

function moveCannon(move) {
    if (cannonIsTryingToRunAway(move)) return;

    switch (true) {
        case cannon.axes.allowX && move == keypad.left:
            clearCannon();
            cannon.x -= cannon.speed;

            //console.log("######### left #########");

            break;
        case cannon.axes.allowY && move == keypad.top:
            clearCannon();
            cannon.y -= cannon.speed;

            //console.log("######### top #########");

            break;
        case cannon.axes.allowX && move == keypad.right:
            clearCannon();
            cannon.x += cannon.speed;

            //console.log("######### right #########");

            break;
        case cannon.axes.allowY && move == keypad.bottom:
            clearCannon();
            cannon.y += cannon.speed;

            //console.log("######### bottom #########");

            break;
        default:
            return;
    }

    drawCannon();
}

function clearCannon() {
    context.clearRect(cannon.x - 1, cannon.y - 1, cannon.width + 1, cannon.height + 2);
}

function cannonIsTryingToRunAway(move) {
    if (move == keypad.left) return (cannon.x - cannon.speed <= 0);
    if (move == keypad.top) return (cannon.y - cannon.speed <= 0);
    if (move == keypad.right) return (cannon.x + cannon.width + cannon.speed >= canvas.width);
    if (move == keypad.bottom) return (cannon.y + cannon.height + cannon.speed >= canvas.height);

    return false;
}

function fireCannon() {
    cannon.shot.onTheWay = true;

    cleanCannonShoot();

    //verifica se a parte de baixo da bala saiu do ecra
    if (cannon.shot.y + cannon.shot.height <= 0) {
        cannonStopShooting();
        return;
    }

    //verifica se esta no estado inicial
    if (cannon.shot.y == 0 && cannon.shot.y == 0) {
        var halfCannonWidth = cannon.width / 2;
        var cannonPistole = cannon.x + halfCannonWidth;

        cannon.shot.x = cannonPistole;
        cannon.shot.y = cannon.y - cannon.shot.height - 5;
    }
    else {
        cannon.shot.y -= cannon.shot.speed;
    }

    //console.log("####### fireCannon draw ####### x:" + cannon.shot.x + "; y:" + cannon.shot.y);

    context.beginPath();
    context.moveTo(cannon.shot.x, cannon.shot.y);
    context.strokeStyle = cannon.shot.color;
    context.lineWidth = cannon.shot.width;
    context.lineTo(cannon.shot.x, cannon.shot.y + cannon.shot.height);
    context.stroke();
    context.closePath();

    cleanInvaderAndStopShootOnKillHim();
}

function cleanCannonShoot() {
    context.clearRect(cannon.shot.x - 1, cannon.shot.y - 1, cannon.shot.width + 1, cannon.shot.height + 1);
}

var movementOrientation = "R";

function setMovementsToInvaders() {
    var maxOnRight = function () {
        var maxRight = 0;

        invaders.forEach(function (item) { if (maxRight < item.x) maxRight = item.x; });

        return maxRight + enemyCannon.width;
    };

    var maxOnLeft = function () {
        var maxLeft = canvas.width;

        invaders.forEach(function (item) { if (maxLeft > item.x) maxLeft = item.x; });

        return maxLeft;
    };

    var canMoveToRight = (maxOnRight() + enemyCannon.speed < canvas.width);
    var canMoveToLeft = (maxOnLeft() - enemyCannon.speed > 0);

    if (movementOrientation == "R" && !canMoveToRight) {
        movementOrientation = "L";
    }
    else if (movementOrientation == "L" && !canMoveToLeft) {
        movementOrientation = "R";
    }

    invaders.forEach(function (item) {
        context.clearRect(item.x, item.y, item.width, item.height); //limpa nave antes de move-la

        if (movementOrientation == "R") {
            item.x += item.speed; //se ja encostou a direita, move para a esquerda

            //console.log("################ setMovementsToInvaders - right ################" + item.x);
        }
        else if (movementOrientation == "L") {
            item.x -= item.speed; //se ja encostou a esquerda, move para a direita

            //console.log("################ setMovementsToInvaders - left ################" + item.x);
        }

        drawEnemy(item);
    });
}

function cleanInvaderAndStopShootOnKillHim() {
    invaders.forEach(function (item) {
        if (cannon.shot.x >= item.x && cannon.shot.x <= item.x + item.width && cannon.shot.y >= item.y && cannon.shot.y <= item.y + item.height) {
            cleanDeadInvader(item);
            cleanCannonShoot();
            cannonStopShooting();

            cannon.score += item.killedPrice;
            header.innerHTML = cannon.score;

            return;
        }
    });

    //todo: dificultar o nivel: mais disparos inimigos e mais movimento
    //recomeca o jogo e ganha uma vida
    if (invaders.length == 0) {
        fillEnemyField(true);
        cannon.lives += 1;
    }
}

function cleanDeadInvader(invader) {
    context.clearRect(invader.x, invader.y, invader.width, invader.height);
    invaders.splice(invaders.indexOf(invader), 1); //ref: http://stackoverflow.com/questions/5767325/remove-specific-element-from-an-array
}

function checkIfInvadersShotCatchedMe() {

}

function setRandomInvadersToShot() {

}

//ref: http://stackoverflow.com/questions/728360/most-elegant-way-to-clone-a-javascript-object
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}